# FAQs for NoteBlockchain

This repo contains a list of common questions related to NoteBlockchain. Deployed as a static site on Netlify, the CMS is Hugo. The contact form raises tickets to a support desk hosted by ngDesk.

## Build status

[![Netlify Status](https://api.netlify.com/api/v1/badges/5a0c697f-b7de-46e6-b386-0087fad735dd/deploy-status)](https://app.netlify.com/sites/pensive-cori-a79f77/deploys)
