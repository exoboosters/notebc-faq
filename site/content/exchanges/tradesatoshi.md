---
title: "TradeSatoshi"
date: 2019-09-30T00:00:00+05:00
type: "post"
author: "The Dark Note"
---

TradeSatoshi added NTBC to their exchange on 30th September 2019. Read their announcement here - https://tradesatoshi.com/News/430/NTBC-Listing

![TradeSatoshi Announcement](../../../../images/tradesatoshi-ann.jpg "tradesatoshi.com")

## Available Pairs

+ NTBC/ETH - https://tradesatoshi.com/Exchange/pro?market=NTBC_ETH
+ NTBC/DOGE - https://tradesatoshi.com/Exchange/pro?market=NTBC_DOGE
+ NTBC/USDT - https://tradesatoshi.com/Exchange/pro?market=NTBC_USDT

## Support/Contact TradeSatoshi

+ Twitter - https://twitter.com/TradeSatoshi
+ Discord - https://discord.gg/96Sc3H6

<div class="alert rounded-0 alert-primary">
  The NoteBlockchain team takes great effort to partner with exchanges that are reputed. However, it is important for users to do their own research when coming to using exchanges. Please do not use exchange wallets as your personal main wallets.
</div>