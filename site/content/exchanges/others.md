---
title: "Other Exchanges"
date: 2019-10-26T00:00:00+05:00
type: "post"
author: "The Dark Note"
---

The NoteBlockchain team is under discussions to add NTBC coin to other exchanges. Due to NDA policies when submitting our listing request, we cannot disclose them until the listing is finalized. 

## Decentralized Exchanges
NoteBlockchain will soon be available on a decentralized exchange. Watch this location for more details.

<div class="alert rounded-0 alert-primary">
  The NoteBlockchain team takes great effort to partner with exchanges that are reputed. However, it is important for users to do their own research when coming to using exchanges. Please do not use exchange wallets as your personal main wallets.
</div>