---
title: "Unnamed.Exchange"
date: 2019-07-12T00:00:00+05:00
type: "post"
author: "The Dark Note"
---

Unnamed.Exchange lets you buy/sell NoteBlockchain for a variety of coins including Unnamed own coin - uTip. Through their trollbox, you can receive tips from any coins listed on the exchange.

![Unnamed Trollbox](../../../../images/unnamed-trollbox.jpg "Unnamed Exchange")

### Available pairs

+ NTBC/BTC - https://www.unnamed.exchange/Exchange?market=NTBC_BTC
+ NTBC/LTC - https://www.unnamed.exchange/Exchange?market=NTBC_LTC
+ NTBC/DOGE - https://www.unnamed.exchange/Exchange?market=NTBC_DOGE
+ NTBC/UTIP - https://www.unnamed.exchange/Exchange?market=NTBC_UTIP

### Support/Contact Unnamed

+ On Twitter - https://twitter.com/exchangeunnamed
+ On Discord - https://discord.gg/phaRuY9

<div class="alert rounded-0 alert-primary">
  The NoteBlockchain team takes great effort to partner with exchanges that are reputed. However, it is important for users to do their own research when coming to using exchanges. Please do not use exchange wallets as your personal main wallets.
</div>